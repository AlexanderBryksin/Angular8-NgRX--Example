import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CustomerState } from '../state/customer.reducer';
import { Store } from '@ngrx/store';
import { UpdateCustomer } from '../state/customer.actions';
import { Observable } from 'rxjs';
import { Customer } from '../customer.model';
import { getCurrentCustomer } from '../state/customer.selectors';

@Component({
  selector: 'app-customer-edit',
  templateUrl: './customer-edit.component.html',
  styleUrls: ['./customer-edit.component.css']
})
export class CustomerEditComponent implements OnInit {
  customerForm: FormGroup;

  constructor(private store: Store<CustomerState>) {}

  ngOnInit() {
    this.customerForm = new FormGroup({
      name: new FormControl(null, Validators.required),
      phone: new FormControl(null, Validators.required),
      address: new FormControl(null, Validators.required),
      membership: new FormControl(null, Validators.required),
      id: new FormControl(null)
    });

    const customer$: Observable<Customer> = this.store.select(getCurrentCustomer);

    customer$.subscribe((currentCustomer: Customer) => {
      if (currentCustomer) {
        this.customerForm.patchValue({
          name: currentCustomer.name,
          phone: currentCustomer.phone,
          address: currentCustomer.address,
          membership: currentCustomer.membership,
          id: currentCustomer.id
        });
      }
    });
  }

  updateCustomer() {
    this.store.dispatch(new UpdateCustomer(this.customerForm.value));
    this.customerForm.reset();
  }
}
