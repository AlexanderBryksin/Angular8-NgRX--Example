import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { CustomerService } from '../customer.service';
import { Observable, of } from 'rxjs';
import { Action } from '@ngrx/store';
import {
  CreateCustomer,
  CreateCustomerError,
  CreateCustomerSuccess,
  CustomerActionTypes,
  DeleteCustomer,
  DeleteCustomerError,
  DeleteCustomerSuccess,
  LoadCustomer,
  LoadCustomerError,
  LoadCustomers,
  LoadCustomersError,
  LoadCustomersSuccess,
  LoadCustomerSuccess,
  UpdateCustomer,
  UpdateCustomerError,
  UpdateCustomerSuccess
} from './customer.actions';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { Customer } from '../customer.model';

@Injectable()
export class CustomerEffects {
  constructor(private actions$: Actions, private customerService: CustomerService) {}

  @Effect()
  loadCustomers$: Observable<Action> = this.actions$.pipe(
    ofType<LoadCustomers>(CustomerActionTypes.LOAD_CUSTOMERS),
    mergeMap((action: LoadCustomers) =>
      this.customerService.getCustomers().pipe(
        map((customers: Customer[]) => new LoadCustomersSuccess(customers)),
        catchError((error) => of(new LoadCustomersError(error)))
      )
    )
  );

  @Effect()
  loadCustomer$: Observable<Action> = this.actions$.pipe(
    ofType<LoadCustomer>(CustomerActionTypes.LOAD_CUSTOMER),
    mergeMap((action: LoadCustomer) =>
      this.customerService.getCustomerById(action.payload).pipe(
        map((customer: Customer) => new LoadCustomerSuccess(customer)),
        catchError((error) => of(new LoadCustomerError(error)))
      )
    )
  );

  @Effect()
  createCustomer$: Observable<Action> = this.actions$.pipe(
    ofType<CreateCustomer>(CustomerActionTypes.CREATE_CUSTOMER),
    mergeMap((action: CreateCustomer) =>
      this.customerService.createCustomer(action.payload).pipe(
        map((newCustomer: Customer) => {
          return new CreateCustomerSuccess(newCustomer);
        }),
        catchError((error) => of(new CreateCustomerError(error)))
      )
    )
  );

  @Effect()
  updateCustomer$: Observable<Action> = this.actions$.pipe(
    ofType<UpdateCustomer>(CustomerActionTypes.UPDATE_CUSTOMER),
    mergeMap((action: UpdateCustomer) =>
      this.customerService.updateCustomer(action.payload).pipe(
        map((updatedCustomer: Customer) => {
          return new UpdateCustomerSuccess({
            id: updatedCustomer.id,
            changes: updatedCustomer
          });
        }),
        catchError((error) => of(new UpdateCustomerError(error)))
      )
    )
  );

  @Effect()
  deleteCustomer$: Observable<Action> = this.actions$.pipe(
    ofType<DeleteCustomer>(CustomerActionTypes.DELETE_CUSTOMER),
    mergeMap((action: DeleteCustomer) =>
      this.customerService.deleteCustomer(action.payload).pipe(
        map((customer: Customer) => {
          return new DeleteCustomerSuccess(action.payload);
        }),
        catchError((error) => of(new DeleteCustomerError(error)))
      )
    )
  );
}
