import { Action } from '@ngrx/store';
import { Customer } from '../customer.model';
import { Update } from '@ngrx/entity';

export enum CustomerActionTypes {
  LOAD_CUSTOMERS = '[Customer] Load Customers',
  LOAD_CUSTOMERS_SUCCESS = '[Customer] Load Customers Success',
  LOAD_CUSTOMERS_ERROR = '[Customer] Load Customers Error',
  LOAD_CUSTOMER = '[Customer] Load Customer',
  LOAD_CUSTOMER_SUCCESS = '[Customer] Load Customer Success',
  LOAD_CUSTOMER_ERROR = '[Customer] Load Customer Error',
  CREATE_CUSTOMER = '[Customer] Create Customer',
  CREATE_CUSTOMER_SUCCESS = '[Customer] Create Customer Success',
  CREATE_CUSTOMER_ERROR = '[Customer] Create Customer Error',
  UPDATE_CUSTOMER = '[Customer] Update Customer',
  UPDATE_CUSTOMER_SUCCESS = '[Customer] Update Customer Success',
  UPDATE_CUSTOMER_ERROR = '[Customer] Update Customer Error',
  DELETE_CUSTOMER = '[Customer] Delete Customer',
  DELETE_CUSTOMER_SUCCESS = '[Customer] Delete Customer Success',
  DELETE_CUSTOMER_ERROR = '[Customer] Delete Customer Error'
}

export class LoadCustomers implements Action {
  readonly type = CustomerActionTypes.LOAD_CUSTOMERS;
}

export class LoadCustomersSuccess implements Action {
  readonly type = CustomerActionTypes.LOAD_CUSTOMERS_SUCCESS;
  constructor(public payload: Customer[]) {}
}

export class LoadCustomersError implements Action {
  readonly type = CustomerActionTypes.LOAD_CUSTOMERS_ERROR;
  constructor(public payload: string) {}
}

export class LoadCustomer implements Action {
  readonly type = CustomerActionTypes.LOAD_CUSTOMER;
  constructor(public payload: string) {}
}

export class LoadCustomerSuccess implements Action {
  readonly type = CustomerActionTypes.LOAD_CUSTOMER_SUCCESS;
  constructor(public payload: Customer) {}
}

export class LoadCustomerError implements Action {
  readonly type = CustomerActionTypes.LOAD_CUSTOMER_ERROR;
  constructor(public payload: string) {}
}

export class CreateCustomer implements Action {
  readonly type = CustomerActionTypes.CREATE_CUSTOMER;
  constructor(public payload: Customer) {}
}

export class CreateCustomerSuccess implements Action {
  readonly type = CustomerActionTypes.CREATE_CUSTOMER_SUCCESS;
  constructor(public payload: Customer) {}
}

export class CreateCustomerError implements Action {
  readonly type = CustomerActionTypes.CREATE_CUSTOMER_ERROR;
  constructor(public payload: string) {}
}

export class UpdateCustomer implements Action {
  readonly type = CustomerActionTypes.UPDATE_CUSTOMER;
  constructor(public payload: Customer) {}
}

export class UpdateCustomerSuccess implements Action {
  readonly type = CustomerActionTypes.UPDATE_CUSTOMER_SUCCESS;
  constructor(public payload: Update<Customer>) {}
}

export class UpdateCustomerError implements Action {
  readonly type = CustomerActionTypes.UPDATE_CUSTOMER_ERROR;
  constructor(public payload: string) {}
}

export class DeleteCustomer implements Action {
  readonly type = CustomerActionTypes.DELETE_CUSTOMER;
  constructor(public payload: string) {}
}

export class DeleteCustomerSuccess implements Action {
  readonly type = CustomerActionTypes.DELETE_CUSTOMER_SUCCESS;
  constructor(public payload: string) {}
}

export class DeleteCustomerError implements Action {
  readonly type = CustomerActionTypes.DELETE_CUSTOMER_ERROR;
  constructor(public payload: string) {}
}

export type Action =
  | LoadCustomers
  | LoadCustomersSuccess
  | LoadCustomersError
  | LoadCustomer
  | LoadCustomerError
  | LoadCustomerSuccess
  | CreateCustomer
  | CreateCustomerSuccess
  | CreateCustomerError
  | UpdateCustomer
  | UpdateCustomerSuccess
  | UpdateCustomerError
  | DeleteCustomer
  | DeleteCustomerSuccess
  | DeleteCustomerError;
