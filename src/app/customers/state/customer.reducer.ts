import { Customer } from '../customer.model';
import * as fromRoot from '../../state/index';
import * as customerActions from './customer.actions';
import { CustomerActionTypes } from './customer.actions';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';

// State interface
export interface CustomerState extends EntityState<Customer> {
  selectedCustomerId: string | null;
  loading: boolean;
  loaded: boolean;
  error: string | null;
}
// Feature interface
export interface CustomersState extends fromRoot.AppState {
  customers: CustomerState;
}

// Entity Adapter
export const customerAdapter: EntityAdapter<Customer> = createEntityAdapter<Customer>();

// Entity state for Adapter
export const defaultCustomer: CustomerState = {
  ids: [],
  entities: {},
  selectedCustomerId: null,
  loaded: false,
  loading: false,
  error: null
};

// Initial State with Entity Adapter
export const initialState = customerAdapter.getInitialState(defaultCustomer);

// Reducer
export function customerReducer(state = initialState, action: customerActions.Action): CustomerState {
  switch (action.type) {
    case CustomerActionTypes.DELETE_CUSTOMER:
    case CustomerActionTypes.UPDATE_CUSTOMER:
    case CustomerActionTypes.CREATE_CUSTOMER:
    case CustomerActionTypes.LOAD_CUSTOMER:
    case CustomerActionTypes.LOAD_CUSTOMERS:
      return {
        ...state,
        loading: true
      };
    case CustomerActionTypes.LOAD_CUSTOMERS_SUCCESS:
      return customerAdapter.addAll(action.payload, {
        ...state,
        loading: false,
        loaded: true,
        error: null
      });
    case CustomerActionTypes.DELETE_CUSTOMER_ERROR:
    case CustomerActionTypes.UPDATE_CUSTOMER_ERROR:
    case CustomerActionTypes.CREATE_CUSTOMER_ERROR:
    case CustomerActionTypes.LOAD_CUSTOMER_ERROR:
    case CustomerActionTypes.LOAD_CUSTOMERS_ERROR:
      return {
        ...state,
        entities: {},
        loaded: false,
        loading: false,
        error: action.payload
      };
    case CustomerActionTypes.LOAD_CUSTOMER_SUCCESS:
      return customerAdapter.addOne(action.payload, {
        ...state,
        selectedCustomerId: action.payload.id,
        loading: false,
        loaded: true,
        error: null
      });
    case CustomerActionTypes.CREATE_CUSTOMER_SUCCESS:
      return customerAdapter.addOne(action.payload, {
        ...state,
        loading: false,
        loaded: true,
        error: null
      });
    case CustomerActionTypes.UPDATE_CUSTOMER_SUCCESS:
      return customerAdapter.updateOne(action.payload, state);
    case CustomerActionTypes.DELETE_CUSTOMER_SUCCESS:
      return customerAdapter.removeOne(action.payload, state);
    default:
      return state;
  }
}
