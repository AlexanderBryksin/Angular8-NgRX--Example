import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Customer } from '../customer.model';
import { DeleteCustomer, LoadCustomer, LoadCustomers, UpdateCustomer } from '../state/customer.actions';
import { Observable } from 'rxjs';
import { CustomerState } from '../state/customer.reducer';
import { getCustomers, getCustomersError } from '../state/customer.selectors';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {
  customers$: Observable<Customer[]>;
  error$: Observable<string>;

  constructor(private store: Store<CustomerState>) {}

  ngOnInit() {
    this.store.dispatch(new LoadCustomers());
    this.customers$ = this.store.pipe(select(getCustomers));
    this.error$ = this.store.pipe(select(getCustomersError));
  }

  editCustomer(customer: Customer) {
    this.store.dispatch(new LoadCustomer(customer.id));
  }

  deleteCustomer(customer: Customer) {
    this.store.dispatch(new DeleteCustomer(customer.id));
  }
}
