import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CustomerState } from '../state/customer.reducer';
import { Store } from '@ngrx/store';
import { CreateCustomer } from '../state/customer.actions';

@Component({
  selector: 'app-customer-add',
  templateUrl: './customer-add.component.html',
  styleUrls: ['./customer-add.component.css']
})
export class CustomerAddComponent implements OnInit {
  customerForm: FormGroup;

  constructor(private store: Store<CustomerState>) {}

  ngOnInit() {
    this.customerForm = new FormGroup({
      name: new FormControl(null, Validators.required),
      phone: new FormControl(null, Validators.required),
      address: new FormControl(null, Validators.required),
      membership: new FormControl(null, Validators.required)
    });
  }

  createCustomer() {
    this.store.dispatch(new CreateCustomer(this.customerForm.value));
    this.customerForm.reset();
  }
}
